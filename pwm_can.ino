#include <FlexCAN_T4.h>
#include <array>
#include "w211_can_b.h"

struct KLA_A1_t KLA_A1; // ECU NAME: KLA_A1, ID: 0x0030, MSG COUNT: 22
struct GW_C_B2_t GW_C_B2; // ECU NAME: GW_C_B2, ID: 0x0003, MSG COUNT: 31
struct SAM_V_A2_t SAM_V_A2; // ECU NAME: SAM_V_A2, ID: 0x0017, MSG COUNT: 4

FlexCAN_T4<CAN0, RX_SIZE_256, TX_SIZE_16> Can0;

byte DUTY_CYCLE = 1;
unsigned int PWM_DUTY = 0;
const unsigned int CANID = 0x0030; // master id
unsigned int CAN_83K3BPS = 83333; // Interior CAN-B Low speed
volatile unsigned long timeout = millis();

template <typename T> void swap(T& var) {
  static_assert(std::is_pod<T>::value, "Type must be POD type for safety");
  std::array<char, sizeof(T)> varArray;
  memcpy(varArray.data(), &var, sizeof(T));
  for(int i = 0; i < static_cast<int>(sizeof(var)/2); i++)
    std::swap(varArray[sizeof(var) - 1 - i],varArray[i]);
  memcpy(&var, varArray.data(), sizeof(T));
}

void blink(unsigned int pin, unsigned int on, unsigned int off)
{
  unsigned int blinkPhase = millis() % (on + off);
  if (blinkPhase < off)
    digitalWrite(pin, LOW);
  else
    digitalWrite(pin, HIGH);
}

void setup()
{
  Serial.begin(115200);
  delay(1000);
  Serial.println("Teensy 3.2 FlexCAN module, CAN0 pins: 3 4, PWM pin: 6");
  Serial.print("baud rate: CAN_83K3BPS = "); Serial.println(CAN_83K3BPS);
  Can0.begin();
  Can0.setBaudRate(CAN_83K3BPS);
  Can0.setMaxMB(16);
  Can0.enableFIFO();
  Can0.enableFIFOInterrupt();
  Can0.setFIFOFilter(REJECT_ALL);
  Can0.setFIFOFilter(0, CANID, STD); // ECU NAME: KLA_A1, ID: 0x0030, MSG COUNT: 22
  Can0.setFIFOFilter(1, 0x0003, STD); // ECU NAME: GW_C_B2, ID: 0x0003, MSG COUNT: 31
  Can0.setFIFOFilter(2, 0x0017, STD); // ECU NAME: SAM_V_A2, ID: 0x0017, MSG COUNT: 4
  Can0.onReceive(canSniff);
  Can0.mailboxStatus();
  analogWriteFrequency(6, 400); // PWM frequency 400 Hz
  pinMode(LED_BUILTIN, OUTPUT);
}

void canSniff(const CAN_message_t &msg)
{
  Serial.print("MB "); Serial.print(msg.mb);
  Serial.print("  OVERRUN: "); Serial.print(msg.flags.overrun);
  Serial.print("  LEN: "); Serial.print(msg.len);
  Serial.print(" EXT: "); Serial.print(msg.flags.extended);
  Serial.print(" TS: "); Serial.print(msg.timestamp);
  Serial.print(" ID: "); Serial.print(msg.id, HEX);
  Serial.print(" Buffer: ");
  for ( uint8_t i = 0; i < msg.len; i++ )
  {
    Serial.print(msg.buf[i], HEX); Serial.print(" ");
  } Serial.println();

  // export CAN_message into bit field decoder
  exportMsg(reinterpret_cast <uint32_t> (msg.id), msg.buf, msg.len);
  if ( msg.id == CANID )
    timeout = millis(); // reset timeout counter for master id
}

// export CAN_message into bit field decoder
void exportMsg(unsigned int id, const uint8_t *msg, uint8_t len)
{
  unsigned short endian16 = 0;
  switch(id) {
    case CANID:
      memcpy(&KLA_A1, msg, len); // ECU NAME: KLA_A1, ID: 0x0030, MSG COUNT: 22
      break;
    case 0x0003:
      memcpy(&GW_C_B2, msg, len); // ECU NAME: GW_C_B2, ID: 0x0003, MSG COUNT: 31
      endian16 = GW_C_B2.DVL;      swap(endian16);      GW_C_B2.DVL = endian16;
      endian16 = GW_C_B2.MBRE_ESP; swap(endian16); GW_C_B2.MBRE_ESP = endian16;
      break;
    case 0x0017:
      memcpy(&SAM_V_A2, msg, len); // ECU NAME: SAM_V_A2, ID: 0x0017, MSG COUNT: 4
      endian16 = SAM_V_A2.P_KAELTE; swap(endian16); SAM_V_A2.P_KAELTE = endian16;
      endian16 = SAM_V_A2.T_KAELTE; swap(endian16); SAM_V_A2.T_KAELTE = endian16;
      break;
  }
}

void _debug_KLA_A1_print(unsigned int delay)
{
  static unsigned long time = 0;
  if ( millis() - time > delay )
  {
    time = millis();
    Serial.println("bitfield 1");
    Serial.print("HHS_EIN = "); Serial.print(KLA_A1.HHS_EIN, DEC); Serial.println(" Heizbare Heckscheibe einschalten");
    Serial.print("EC_AKT = "); Serial.print(KLA_A1.EC_AKT, DEC); Serial.println(" EC-Modus aktiv");
    Serial.print("IFG_EIN = "); Serial.print(KLA_A1.IFG_EIN, DEC); Serial.println(" Innenfühlergebläse einschalten");
    Serial.print("ZWP_EIN = "); Serial.print(KLA_A1.ZWP_EIN, DEC); Serial.println(" Zusatzwasserpumpe einschalten");
    Serial.print("ZH_EIN_OK = "); Serial.print(KLA_A1.ZH_EIN_OK, DEC); Serial.println(" Zuheizer einschalten erlaubt");
    Serial.print("LL_DZA = "); Serial.print(KLA_A1.LL_DZA, DEC); Serial.println(" Leerlauf-Drehzahlanhebung zur Kälteleistungserhöhung");
    Serial.print("HEIZEN = "); Serial.print(KLA_A1.HEIZEN, DEC); Serial.println(" Standheizung heizen");
    Serial.print("LUEFTEN = "); Serial.print(KLA_A1.LUEFTEN, DEC); Serial.println(" Standheizung lüften");
    Serial.println("byte 2, 3, 4");
    Serial.print("NLFTS = "); Serial.print(KLA_A1.NLFTS, DEC); Serial.println(" (%) Motorlüfter Solldrehzahl");
    Serial.print("M_KOMP = "); Serial.print(KLA_A1.M_KOMP * 3, DEC); Serial.println(" (Nm) Drehmomentaufnahme Kältekompressor");
    Serial.print("KOMP_STELL = "); Serial.print(KLA_A1.KOMP_STELL, DEC); Serial.println(" (%) Kältekompressor Stellsignal");
    Serial.println("bitfield 5");
    Serial.print("DEFROST_AKT = "); Serial.print(KLA_A1.DEFROST_AKT, DEC); Serial.println(" Defrostbetrieb aktiv");
    Serial.print("REST_AKT = "); Serial.print(KLA_A1.REST_AKT, DEC); Serial.println(" Restwärmebetrieb aktiv");
    Serial.print("KJAL_ZU = "); Serial.print(KLA_A1.KJAL_ZU, DEC); Serial.println(" Kühlerjalousie schließe");
    Serial.print("ABVENT_W_ZU = "); Serial.print(KLA_A1.ABVENT_W_ZU, DEC); Serial.println(" Absperrventil Wärmetauscher schließen");
    Serial.print("ZH_ANF = "); Serial.print(KLA_A1.ZH_ANF, DEC); Serial.println(" (Stufen) Anforderung Zuheizleistung");
    Serial.println("byte 6");
    Serial.print("GEB_LSTG = "); Serial.print(KLA_A1.GEB_LSTG, DEC); Serial.println(" (%) Gebläse-Leistung");
    Serial.println("bitfield 7");
    Serial.print("UL_AKT_KLA = "); Serial.print(KLA_A1.UL_AKT_KLA, DEC); Serial.println(" Umluft aktiv");
    Serial.print("LKO_VORN = "); Serial.print(KLA_A1.LKO_VORN, DEC); Serial.println(" Stellung Lüftungsklappe oben");
    Serial.print("LKM_VORN = "); Serial.print(KLA_A1.LKM_VORN, DEC); Serial.println(" Stellung Lüftungsklappe Mitte");
    Serial.print("LKU_VORN = "); Serial.print(KLA_A1.LKU_VORN, DEC); Serial.println(" Stellung Lüftungsklappe unten");
    Serial.println("byte 8");
    Serial.print("T_INNEN_KLA = "); Serial.print(KLA_A1.T_INNEN_KLA * 0.25, DEC); Serial.println(" (°C) Innentemperatur");
  }
}

void _debug_GW_C_B2_print(unsigned int delay)
{
  static unsigned long time = 0;
  if ( millis() - time > delay )
  {
    time = millis();
    Serial.println("bitfield 1");
    Serial.print("ART_ABW_AKT = "); Serial.print(GW_C_B2.ART_ABW_AKT, DEC); Serial.println(" ART-Abstandswarnung ist eingeschaltet");
    Serial.print("KOMP_BAUS = "); Serial.print(GW_C_B2.KOMP_BAUS, DEC); Serial.println(" Klima-Kompressor ausschalten: Beschleunigung");
    Serial.print("KOMP_NOTAUS = "); Serial.print(GW_C_B2.KOMP_NOTAUS, DEC); Serial.println(" Klima-Kompressor Not-Ausschalten");
    Serial.print("LUEFT_MOT_KL = "); Serial.print(GW_C_B2.LUEFT_MOT_KL, DEC); Serial.println(" Motorlüfter defekt Kontrolllampe");
    Serial.print("BRE_AKT_ESP = "); Serial.print(GW_C_B2.BRE_AKT_ESP, DEC); Serial.println(" ESP-Bremseneingriff aktiv");
    Serial.print("BLS_UNT = "); Serial.print(GW_C_B2.BLS_UNT, DEC); Serial.println(" Bremslichtunterdrückung");
    Serial.print("BLS_ST = "); Serial.print(GW_C_B2.BLS_ST, DEC); Serial.println(" Status Bremslichtschalter");
    Serial.println("bitfield 2");
    Serial.print("RG = "); Serial.print(GW_C_B2.RG, DEC); Serial.println(" Rückwärtsgang eingelegt (alle Getriebe)");
    Serial.print("P = "); Serial.print(GW_C_B2.P, DEC); Serial.println(" Parkstellung eingelegt");
    Serial.print("BELADUNG = "); Serial.print(GW_C_B2.BELADUNG, DEC); Serial.println(" Beladung");
    Serial.print("WHC = "); Serial.print(GW_C_B2.WHC, DEC); Serial.println(" Getriebewählhebelstellung (nur NAG)");
    Serial.println("bitfield 3+4");
    Serial.print("DRTGVL = "); Serial.print(GW_C_B2.DRTGVL, DEC); Serial.println(" Drehrichtung Rad vorne links");
    Serial.print("DVL = "); Serial.print(GW_C_B2.DVL * 0.5, DEC); Serial.println(" (1/min) Raddrehzahl vorne links");
    Serial.println("bitfield 5");
    Serial.print("ST2_LED_DL = "); Serial.print(GW_C_B2.ST2_LED_DL, DEC); Serial.println(" LED 2-stufiger Schalter Dauerlicht");
    Serial.print("ST3_LEDR_BL = "); Serial.print(GW_C_B2.ST3_LEDR_BL, DEC); Serial.println(" Rechte LED 3-stufiger Schalter Blinklicht");
    Serial.print("ST3_LEDR_DL = "); Serial.print(GW_C_B2.ST3_LEDR_DL, DEC); Serial.println(" Rechte LED 3-stufiger Schalter Dauerlicht");
    Serial.print("ST3_LEDL_BL = "); Serial.print(GW_C_B2.ST3_LEDL_BL, DEC); Serial.println(" Linke LED 3-stufiger Schalter Blinklicht");
    Serial.print("ST3_LEDL_DL = "); Serial.print(GW_C_B2.ST3_LEDL_DL, DEC); Serial.println(" Linke LED 3-stufiger Schalter Dauerlicht");
    Serial.print("INF_RFE_FSG = "); Serial.print(GW_C_B2.INF_RFE_FSG, DEC); Serial.println(" FSG: EHB-ASG in Rückfallebene");
    Serial.print("KPL = "); Serial.print(GW_C_B2.KPL, DEC); Serial.println(" Kupplung getreten");
    Serial.print("LTG_CHK_POS = "); Serial.print(GW_C_B2.LTG_CHK_POS, DEC); Serial.println(" Leitungsüberwachung möglich");
    Serial.println("bitfield 6+7");
    Serial.print("WHST = "); Serial.print(GW_C_B2.WHST, DEC); Serial.println(" Getriebewählhebelstellung (NAG, KSG, CVT)");
    Serial.print("MBRE_ESP = "); Serial.print(GW_C_B2.MBRE_ESP * 3, DEC); Serial.println(" (Nm) Eingestelltes Bremsmoment");
    Serial.println("bitfield 8");
    Serial.print("ZWP_EIN_MS = "); Serial.print(GW_C_B2.ZWP_EIN_MS, DEC); Serial.println(" Zusatzwasserpumpe einschalten");
    Serial.print("ZH_AUS_MS = "); Serial.print(GW_C_B2.ZH_AUS_MS, DEC); Serial.println(" Anforderung PTC-Zuheizer aus");
    Serial.print("N_VRBT_SBCSH_AKT = "); Serial.print(GW_C_B2.N_VRBT_SBCSH_AKT, DEC); Serial.println(" SBC-S/H aktiv, ASG darf nicht nach \"N\" schalten");
    Serial.print("NOTBRE = "); Serial.print(GW_C_B2.NOTBRE, DEC); Serial.println(" Notbremsung (Bremslicht blinken)");
    Serial.print("ZVB_EIN_MS = "); Serial.print(GW_C_B2.ZVB_EIN_MS, DEC); Serial.println(" Zusatzverbraucher einschalten");
    Serial.print("SUB_ABL_L = "); Serial.print(GW_C_B2.SUB_ABL_L, DEC); Serial.println(" Substitution Abblendlicht links");
    Serial.print("SUB_ABL_R = "); Serial.print(GW_C_B2.SUB_ABL_R, DEC); Serial.println(" Substitution Abblendlicht rechts");
    Serial.print("LL_STBL = "); Serial.print(GW_C_B2.LL_STBL, DEC); Serial.println(" Leerlauf ist stabil");
  }
}

void _debug_SAM_V_A2_print(unsigned int delay)
{
  static unsigned long time = 0;
  if ( millis() - time > delay )
  {
    time = millis();
    Serial.println("byte 1, 2+3, 4+5, 6");
    Serial.print("T_AUSSEN_B = "); Serial.print(SAM_V_A2.T_AUSSEN_B * 0.5 - 40, DEC); Serial.println(" (°C) Außenlufttemperatur");
    Serial.print("P_KAELTE = "); Serial.print(SAM_V_A2.P_KAELTE * 0.1, DEC); Serial.println(" (bar) Druck Kältemittel R134a");
    Serial.print("T_KAELTE = "); Serial.print(SAM_V_A2.T_KAELTE * 0.1 - 10, DEC); Serial.println(" (°C) Temperatur Kältemittel R134a");
    Serial.print("I_KOMP = "); Serial.print(SAM_V_A2.I_KOMP * 10, DEC); Serial.println(" (mA) Strom Kompressor-Hauptregelventil");
  }
}

void SerialPrintWarn(String text, unsigned int value, unsigned int delay)
{
  static unsigned long time = 0;
  if ( millis() - time > delay )
  {
    time = millis();
    Serial.print(text); Serial.println(value, DEC);
  }
}

void loop()
{
  // Important: we use memcpy to fill volatile global structs from exportMsg()
  Can0.events();

  // PWM off lost connection (master id)
  if ( millis() - timeout > 5000 )
  {
    Serial.print("CAN0 pin 4: timeout ... no traffic for CAN-ID: 0x"); Serial.println(CANID, HEX);
    DUTY_CYCLE = 0;
    PWM_DUTY = 0;
    timeout = millis();
  }
    // PWM set new duty cycle on change only
    else if ( DUTY_CYCLE != KLA_A1.KOMP_STELL )
    {
      Serial.print("Refrigeration compressor control signal (%): KOMP_STELL = "); Serial.println(KLA_A1.KOMP_STELL, DEC);

      if ( KLA_A1.KOMP_STELL > 100 )
        DUTY_CYCLE = 0;
      else
        DUTY_CYCLE = KLA_A1.KOMP_STELL;

      PWM_DUTY = DUTY_CYCLE * 255;
      PWM_DUTY = PWM_DUTY / 100;
    }

  // emergency override control signals
  if ( GW_C_B2.KOMP_BAUS )
  {
    SerialPrintWarn("Refrigeration compressor turn off: Acceleration: KOMP_BAUS = ", GW_C_B2.KOMP_BAUS, 1000);
    DUTY_CYCLE = 0;
    PWM_DUTY = 0;
  }
  if ( GW_C_B2.KOMP_NOTAUS )
  {
    SerialPrintWarn("Refrigeration compressor: Emergency Shutdown: KOMP_NOTAUS = ", GW_C_B2.KOMP_NOTAUS, 1000);
    DUTY_CYCLE = 0;
    PWM_DUTY = 0;
  }
  if ( SAM_V_A2.P_KAELTE < 8 ) // min 0.8 bar
  {
    SerialPrintWarn("Refrigerant R134a pressure too low (bar): P_KAELTE = ", SAM_V_A2.P_KAELTE * 0.1, 1000);
    DUTY_CYCLE = 0;
    PWM_DUTY = 0;
  }
  if ( SAM_V_A2.P_KAELTE > 280 ) // max 28.0 bar
  {
    SerialPrintWarn("Refrigerant R134a pressure too high (bar): P_KAELTE = ", SAM_V_A2.P_KAELTE * 0.1, 1000);
    DUTY_CYCLE = 0;
    PWM_DUTY = 0;
  }

/*
  // DEBUG
  _debug_KLA_A1_print(1000);
  _debug_GW_C_B2_print(1000);
  _debug_SAM_V_A2_print(1000);
*/
  // PWM start duty cycle
  analogWrite(6, PWM_DUTY);
 
  // LED start blinking
  if ( KLA_A1.EC_AKT )
    blink(LED_BUILTIN, 100, 1900); // AC off
  else
    blink(LED_BUILTIN, 20, 80); // AC on
}
